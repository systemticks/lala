// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { LanguageClientOptions } from 'vscode-languageclient';
import { LanguageClient, ServerOptions, StreamInfo } from 'vscode-languageclient/node';
import path = require('path');

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	const executable = process.platform === 'win32' ? 'lala-server.bat' : 'lala-server';
	const languageServerPath = path.join('server', 'lala-server', 'bin', executable);
	const serverLauncher = context.asAbsolutePath(languageServerPath);
	
	const clientOptions: LanguageClientOptions = {
		documentSelector: [{ scheme: 'file', language: 'lala' }],
		synchronize: {
			fileEvents: vscode.workspace.createFileSystemWatcher('**/*.lala')
		}
	};

	const getServerOptions = function (): ServerOptions {
		return {
			run: {
				command: serverLauncher,
			},
			debug: {
				command: serverLauncher,
			}
		}
	}

	const languageClient = new LanguageClient('Lala Client', 'Lala Server', getServerOptions(), clientOptions);
	
	languageClient.start();

	//context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() { }
