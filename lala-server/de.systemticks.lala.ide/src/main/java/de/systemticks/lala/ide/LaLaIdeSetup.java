/*
 * generated by Xtext 2.29.0
 */
package de.systemticks.lala.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.systemticks.lala.LaLaRuntimeModule;
import de.systemticks.lala.LaLaStandaloneSetup;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
public class LaLaIdeSetup extends LaLaStandaloneSetup {

	@Override
	public Injector createInjector() {
		return Guice.createInjector(Modules2.mixin(new LaLaRuntimeModule(), new LaLaIdeModule()));
	}
	
}
