package de.systemticks.lala.formatting2

import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument
import de.systemticks.lala.laLa.LanguageDefinitions
import de.systemticks.lala.laLa.Language
import de.systemticks.lala.laLa.TranslationGroups
import de.systemticks.lala.laLa.TranslatableOrGroup
import de.systemticks.lala.laLa.Translatable
import de.systemticks.lala.laLa.Translation
import de.systemticks.lala.laLa.TranslatableGroup

class LaLaFormatter extends AbstractFormatter2 {

	def dispatch void format(LanguageDefinitions languageDefinition, extension IFormattableDocument document) {
		languageDefinition.prepend[noSpace]
		val open = languageDefinition.regionFor.keyword("{")
		val close = languageDefinition.regionFor.keyword("}")
		open.prepend[oneSpace]
		open.append[noSpace]
		open.append[newLine]
		interior(open, close)[indent]
		close.prepend[newLine]
		languageDefinition.languages.forEach[it.format(document)]
	}
	
	def dispatch void format(Language language, extension IFormattableDocument document) {
		language.append[newLine]
		language.regionFor.keyword("as").surround[oneSpace]
		language.regionFor.keyword("or").surround[oneSpace]
		language.append[noSpace]		
	}
	
	def dispatch void format(TranslationGroups translationGroups, extension IFormattableDocument document) {
		translationGroups.prepend[newLine]
		val open = translationGroups.regionFor.keyword("{")
		val close = translationGroups.regionFor.keyword("}")
		open.prepend[oneSpace]
		open.append[noSpace]
		open.append[newLine]
		interior(open, close)[indent]
		close.prepend[newLine]
		translationGroups.translatableOrGroup.format(document)
	}
	
	def dispatch void format(TranslatableOrGroup translatableGroup, extension IFormattableDocument document) {
		translatableGroup.translatables.forEach[it.format(document)]
		translatableGroup.groups.forEach[it.format(document)]
	}
	
	def dispatch void format(Translatable translatable, extension IFormattableDocument document) {
		translatable.prepend[newLine]
		val open = translatable.regionFor.keyword("[")
		val close = translatable.regionFor.keyword("]")
		open.prepend[oneSpace]
		open.append[noSpace]
		open.append[newLine]
		interior(open, close)[indent]
		close.prepend[newLine]
		translatable.translations.forEach[it.format(document)]
	}
	
	def dispatch void format(Translation translation, extension IFormattableDocument document) {
		translation.append[newLine]
		translation.regionFor.keyword("=").surround[oneSpace]
		translation.append[noSpace]		
	}
	
	def dispatch void format(TranslatableGroup translatableGroup, extension IFormattableDocument document) {
		translatableGroup.prepend[newLine]
		val open = translatableGroup.regionFor.keyword("{")
		val close = translatableGroup.regionFor.keyword("}")
		open.prepend[oneSpace]
		open.append[noSpace]
		open.append[newLine]
		interior(open, close)[indent]
		close.prepend[newLine]
		translatableGroup.translatableOrGroup.format(document)
	}
}
