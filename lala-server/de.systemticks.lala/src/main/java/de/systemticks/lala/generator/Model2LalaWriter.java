package de.systemticks.lala.generator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import de.systemticks.lala.LaLaStandaloneSetup;
import de.systemticks.lala.laLa.Model;

public class Model2LalaWriter {
    
    String transformToLalaAsString(Model model, String filename) throws IOException {

        URI uri = URI.createFileURI(filename);
        Injector injector = new LaLaStandaloneSetup().createInjectorAndDoEMFRegistration();

        XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
        Resource resource = resourceSet.createResource(uri);
        resource.getContents().add(model);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        resource.save(baos, Collections.emptyMap());

        return new String(baos.toByteArray());
    }

}
