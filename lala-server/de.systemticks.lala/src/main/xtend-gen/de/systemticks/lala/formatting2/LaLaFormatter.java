package de.systemticks.lala.formatting2;

import de.systemticks.lala.laLa.Language;
import de.systemticks.lala.laLa.LanguageDefinitions;
import de.systemticks.lala.laLa.Translatable;
import de.systemticks.lala.laLa.TranslatableGroup;
import de.systemticks.lala.laLa.TranslatableOrGroup;
import de.systemticks.lala.laLa.Translation;
import de.systemticks.lala.laLa.TranslationGroups;
import java.util.Arrays;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.formatting2.AbstractFormatter2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.formatting2.IHiddenRegionFormatter;
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class LaLaFormatter extends AbstractFormatter2 {
  protected void _format(final LanguageDefinitions languageDefinition, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.<LanguageDefinitions>prepend(languageDefinition, _function);
    final ISemanticRegion open = this.textRegionExtensions.regionFor(languageDefinition).keyword("{");
    final ISemanticRegion close = this.textRegionExtensions.regionFor(languageDefinition).keyword("}");
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(open, _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.append(open, _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(open, _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(open, close, _function_4);
    final Procedure1<IHiddenRegionFormatter> _function_5 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.prepend(close, _function_5);
    final Consumer<Language> _function_6 = (Language it) -> {
      this.format(it, document);
    };
    languageDefinition.getLanguages().forEach(_function_6);
  }

  protected void _format(final Language language, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.<Language>append(language, _function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(language).keyword("as"), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(language).keyword("or"), _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.<Language>append(language, _function_3);
  }

  protected void _format(final TranslationGroups translationGroups, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.<TranslationGroups>prepend(translationGroups, _function);
    final ISemanticRegion open = this.textRegionExtensions.regionFor(translationGroups).keyword("{");
    final ISemanticRegion close = this.textRegionExtensions.regionFor(translationGroups).keyword("}");
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(open, _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.append(open, _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(open, _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(open, close, _function_4);
    final Procedure1<IHiddenRegionFormatter> _function_5 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.prepend(close, _function_5);
    this.format(translationGroups.getTranslatableOrGroup(), document);
  }

  protected void _format(final TranslatableOrGroup translatableGroup, @Extension final IFormattableDocument document) {
    final Consumer<Translatable> _function = (Translatable it) -> {
      this.format(it, document);
    };
    translatableGroup.getTranslatables().forEach(_function);
    final Consumer<TranslatableGroup> _function_1 = (TranslatableGroup it) -> {
      this.format(it, document);
    };
    translatableGroup.getGroups().forEach(_function_1);
  }

  protected void _format(final Translatable translatable, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.<Translatable>prepend(translatable, _function);
    final ISemanticRegion open = this.textRegionExtensions.regionFor(translatable).keyword("[");
    final ISemanticRegion close = this.textRegionExtensions.regionFor(translatable).keyword("]");
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(open, _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.append(open, _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(open, _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(open, close, _function_4);
    final Procedure1<IHiddenRegionFormatter> _function_5 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.prepend(close, _function_5);
    final Consumer<Translation> _function_6 = (Translation it) -> {
      this.format(it, document);
    };
    translatable.getTranslations().forEach(_function_6);
  }

  protected void _format(final Translation translation, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.<Translation>append(translation, _function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(translation).keyword("="), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.<Translation>append(translation, _function_2);
  }

  protected void _format(final TranslatableGroup translatableGroup, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.<TranslatableGroup>prepend(translatableGroup, _function);
    final ISemanticRegion open = this.textRegionExtensions.regionFor(translatableGroup).keyword("{");
    final ISemanticRegion close = this.textRegionExtensions.regionFor(translatableGroup).keyword("}");
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(open, _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.append(open, _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(open, _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(open, close, _function_4);
    final Procedure1<IHiddenRegionFormatter> _function_5 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.prepend(close, _function_5);
    this.format(translatableGroup.getTranslatableOrGroup(), document);
  }

  public void format(final Object language, final IFormattableDocument document) {
    if (language instanceof XtextResource) {
      _format((XtextResource)language, document);
      return;
    } else if (language instanceof Language) {
      _format((Language)language, document);
      return;
    } else if (language instanceof LanguageDefinitions) {
      _format((LanguageDefinitions)language, document);
      return;
    } else if (language instanceof Translatable) {
      _format((Translatable)language, document);
      return;
    } else if (language instanceof TranslatableGroup) {
      _format((TranslatableGroup)language, document);
      return;
    } else if (language instanceof TranslatableOrGroup) {
      _format((TranslatableOrGroup)language, document);
      return;
    } else if (language instanceof Translation) {
      _format((Translation)language, document);
      return;
    } else if (language instanceof TranslationGroups) {
      _format((TranslationGroups)language, document);
      return;
    } else if (language instanceof EObject) {
      _format((EObject)language, document);
      return;
    } else if (language == null) {
      _format((Void)null, document);
      return;
    } else if (language != null) {
      _format(language, document);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(language, document).toString());
    }
  }
}
