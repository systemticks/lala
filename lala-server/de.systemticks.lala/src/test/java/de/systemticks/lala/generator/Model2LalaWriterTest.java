package de.systemticks.lala.generator;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.systemticks.lala.laLa.LaLaFactory;
import de.systemticks.lala.laLa.Language;
import de.systemticks.lala.laLa.LanguageDefinitions;
import de.systemticks.lala.laLa.Model;
import de.systemticks.lala.laLa.Translatable;
import de.systemticks.lala.laLa.TranslatableOrGroup;
import de.systemticks.lala.laLa.Translation;
import de.systemticks.lala.laLa.TranslationGroups;
import de.systemticks.lala.laLa.TranslationValue;

public class Model2LalaWriterTest {
    
    private Model testModel;
    private Model2LalaWriter writer;
    private static final String EXPECTED_LALA = "languages { en_US as EN de_DE as DE or EN } translations { Station [ EN = \"Bahnhof\" DE = \"Station\" ] }";

    @BeforeEach
    public void setup() {
        testModel = createTestModel();
        writer = new Model2LalaWriter();
    }

    @Test
    public void transformToLalaText() {
        assertAll(
            () -> assertEquals(EXPECTED_LALA, writer.transformToLalaAsString(testModel, "result.lala"))
        );
    }

    private Model createTestModel() {

        Model model = LaLaFactory.eINSTANCE.createModel();

        Language language1 = LaLaFactory.eINSTANCE.createLanguage();
        language1.setName("EN");
        language1.setLocaleCode("en_US");

        Language language2 = LaLaFactory.eINSTANCE.createLanguage();
        language2.setName("DE");
        language2.setLocaleCode("de_DE");
        language2.setFallback(language1);

        LanguageDefinitions languageDefs = LaLaFactory.eINSTANCE.createLanguageDefinitions();
        languageDefs.getLanguages().add(language1);
        languageDefs.getLanguages().add(language2);

        model.setLanguageDefinition(languageDefs);

        Translation translation1 = LaLaFactory.eINSTANCE.createTranslation();
        translation1.setLanguage(language1);
        TranslationValue value1 = LaLaFactory.eINSTANCE.createTranslationValue();
        value1.setText("Bahnhof");
        translation1.getValue().add(value1);

        Translation translation2 = LaLaFactory.eINSTANCE.createTranslation();
        translation2.setLanguage(language2);
        TranslationValue value2 = LaLaFactory.eINSTANCE.createTranslationValue();
        value2.setText("Station");
        translation2.getValue().add(value2);

        Translatable translatable = LaLaFactory.eINSTANCE.createTranslatable();
        translatable.getTranslations().add(translation1);
        translatable.getTranslations().add(translation2);
        translatable.setName("Station");

        TranslatableOrGroup translatableOrGroup = LaLaFactory.eINSTANCE.createTranslatableOrGroup();
        translatableOrGroup.getTranslatables().add(translatable);

        TranslationGroups translationGroups = LaLaFactory.eINSTANCE.createTranslationGroups();
        translationGroups.setTranslatableOrGroup(translatableOrGroup);

        model.setTranslationGroups(translationGroups);

        return model;

    }

}
